import glob
import os

import MySQLdb
from datetime import datetime, timedelta
import sys
import subprocess
import logging
import ConfigParser

import errno

config = ConfigParser.ConfigParser()
config.readfp(open(r'config.ini'))
path_save_turkic_files = config.get('paths', 'path_save_turkic_files')


def createFolder(path):
    if not os.path.exists(path):
        subprocess.call(['sudo', 'mkdir', path])

def turkicDumpCommand(videoName):
    cmdLine = "cd /root/vatic ; sudo turkic dump " + videoName +  " -o " + path_save_turkic_files +  "/" + videoName + ".txt"
    p = subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
    p.communicate(b"input data that is passed to subprocess' stdin")


def turkicVideos(videoNames):
    video_name = ""
    for i in range(0, len(videoNames)):
        if video_name != videoNames[i]:
            turkicDumpCommand(videoNames[i])
            video_name = videoNames[i]

def findVideosDates(date):

    command = "select date(t.timecompleted) from videos v join segments s on v.id=s.videoid join jobs j  on j.segmentid=s.id" \
              " join turkic_hits t on j.id=t.id where cast(t.timecompleted as DATE) < '" + date + "' group by date(timecompleted)"
    db = MySQLdb.connect("localhost", "root", "viisights5344", "vatic")
    db.query(command)

    data = db.store_result()
    data = data.fetch_row(0)

    videoDatesList = list(data)
    for i in range(0, len(videoDatesList)):
        videoDatesList[i] = videoDatesList[i][0]

    db.close()
    logging.debug("findVideosDates: cmdLine: %s", command)

    return videoDatesList

def findVideosNameByDate(date):
    command = "select slug from videos v join segments s on v.id=s.videoid join jobs j  on j.segmentid=s.id join" \
              " turkic_hits t on j.id=t.id where cast(t.timecompleted as DATE)= '" + date + "'"

    db = MySQLdb.connect("localhost", "root", "viisights5344", "vatic")
    db.query(command)

    data = db.store_result()
    data = data.fetch_row(0)

    videoNamesList = list(data)
    for i in range(0, len(videoNamesList)):
        videoNamesList[i] = videoNamesList[i][0]

    db.close()

    logging.debug("findVideosNameByDate: sql command: %s", command)
    return videoNamesList

def find(date, merge_rate, part, logname, date_of_exec):
    cmdLine = "python validate_daily_tagging.py " + date + " " + merge_rate + " part_" + str(part) + " " + logname + " " + date_of_exec
    p = subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)
    p.communicate(b"input data that is passed to subprocess' stdin")
    logging.debug("findVideosDates: cmdLine: %s", cmdLine)


def getVideoName(name):
    list = name.split('/')
    name = list[-1]
    return name[:-4]


def delete_temp_files():
    try:
        cmdLine = "sudo rm -rf " + path_save_turkic_files + "/*"
        subprocess.Popen(cmdLine, shell=True, stdout=subprocess.PIPE)

        logging.debug("delete_temp_files: deleted successfully")
    except:
        logging.debug("delete_temp_files: delete failed")


def readFile(txtFile, attribute, video_name):
    lines = txtFile.read().split('\n')
    count = 0
    for line in lines:
        thisLine = line.split(' ')
        for i in range(9, len(thisLine)):
            check_line = thisLine[i]
            check_line = check_line.translate(None,'"')
            check_line = check_line.translate(None,"'")
            #print "attribute: %s  check_line: %s" %(attribute, check_line)
            if attribute == check_line:
                count += 1
                break

    if count > 0:
        print "%s - %s " %(video_name, count)

print "welcome to find_attributes"
bol_torkic = raw_input("would you like to rescan all videos? (around 20 min work?), answer in yes/no ")

startScriptTime = datetime.now()

date = datetime.today() + timedelta(2)
date = str(date)
videoDatesList = findVideosDates(date)
createFolder("/tmp")
createFolder(path_save_turkic_files)

if bol_torkic == 'yes':
    print "turcking all videos, please wait"
    for i in range(0, len(videoDatesList)):   ## main loop
        video_List = findVideosNameByDate(str(videoDatesList[i]))
        turkicVideos(video_List)
        num = float( float(i) / float(len(videoDatesList))) * 100
        print "finished " + str(int(num)) + "% videos"

print "\n"
while True:
    attribute = str(raw_input("enter attribute to search, to end write 'end' "))
    if attribute == "end":
        exit()
    print "searching attribute: %s" %attribute
    print "video name,  number of occurrences"
    files = glob.glob(path_save_turkic_files + "/*.txt")
    for name in files:  # 'file' is a builtin type, 'name' is a less-ambiguous variable name.
        try:
            with open(name) as f:  # No need to specify 'r': this is the default.
                fileName = getVideoName(name)
                logging.info("opening txt file for reading: %s", str(fileName))
                readFile(f, attribute, fileName)

        except IOError as exc:
            if exc.errno != errno.EISDIR:  # Do not fail if a directory is found, just ignore it.
                raise  # Propagate other kinds of IOError.

#delete_temp_files()

runningTime = str(datetime.now() - startScriptTime)
logging.info("script ended after %s" ,runningTime)
